package dev.stjiggyfly.filedb.responses;

import dev.stjiggyfly.filedb.core.Mappable;
import dev.stjiggyfly.filedb.core.ResponseStatus;
import dev.stjiggyfly.filedb.core.ResultSet;
import dev.stjiggyfly.filedb.exceptions.FileDBException;
import dev.stjiggyfly.filedb.requests.DatabaseRequest;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DatabaseResponse
{
	private DatabaseRequest request;
	private ResponseStatus status;
	private String errorMessage;
	private ResultSet resultSet;

	public DatabaseRequest getRequest ()
	{
		return request;
	}

	public void setRequest ( DatabaseRequest request )
	{
		this.request = request;
	}

	public ResponseStatus getStatus ()
	{
		return status;
	}

	public void setStatus ( ResponseStatus status )
	{
		this.status = status;
	}

	public String getErrorMessage ()
	{
		return errorMessage;
	}

	public void setErrorMessage ( String errorMessage )
	{
		this.errorMessage = errorMessage;
	}

	public ResultSet getResultSet ()
	{
		return resultSet;
	}

	public void setResultSet ( ResultSet resultSet )
	{
		this.resultSet = resultSet;
	}

	public List<String> mapStrings ()
	{
		List<String> returnList = new ArrayList<>();
		while ( resultSet.next() )
		{
			returnList.add( resultSet.getString( 0 ) );
		}
		return returnList;
	}

	public String mapString ()
	{
		List<String> strings = mapStrings();
		if ( strings.size() == 0 || strings.size() > 1 )
		{
			throw new FileDBException( "Incorrect result size, expected 1 : found " + strings.size() );
		}
		return strings.get( 0 );
	}

	public List<BigDecimal> mapBigDecimals ()
	{
		List<BigDecimal> returnList = new ArrayList<>();
		while ( resultSet.next() )
		{
			try
			{
				returnList.add( resultSet.getBigDecimal( 0 ) );
			}
			catch ( Exception e )
			{
				throw new FileDBException( "Results not mappable to BigDecimal type" );
			}
		}
		return returnList;
	}

	public BigDecimal mapBigDecimal ()
	{
		List<BigDecimal> bigDecimals = mapBigDecimals();
		if ( bigDecimals.size() == 0 || bigDecimals.size() > 1 )
		{
			throw new FileDBException( "Incorrect result size, expected 1 : found " + bigDecimals.size() );
		}
		return bigDecimals.get( 0 );
	}

	public List<Integer> mapInts ()
	{
		List<Integer> returnList = new ArrayList<>();
		while ( resultSet.next() )
		{
			try
			{
				returnList.add( resultSet.getInt( 0 ) );
			}
			catch ( Exception e )
			{
				throw new FileDBException( "Results not mappable to Integer type" );
			}
		}
		return returnList;
	}

	public Integer mapInt ()
	{
		List<Integer> ints = mapInts();
		if ( ints.size() == 0 || ints.size() > 1 )
		{
			throw new FileDBException( "Incorrect result size, expected 1 : found " + ints.size() );
		}
		return ints.get( 0 );
	}

	@SuppressWarnings( "unchecked" )
	public <T extends Mappable> List<T> mapObjects ( Class<T> clazz )
	{
		List<T> returnList = new ArrayList<>();
		while ( resultSet.next() )
		{
			try
			{
				returnList.add( (T) ( (Mappable) Class.forName( clazz.getName() ).getConstructor().newInstance() ).map( resultSet ) );
			}
			catch ( Exception e )
			{
				throw new FileDBException( "Unable to map list of objects of type " + clazz.getName() );
			}
		}
		return returnList;
	}

	public <T extends Mappable> T mapObject ( Class<T> clazz )
	{
		List<T> mappables = mapObjects( clazz );
		if ( mappables.size() == 0 || mappables.size() > 1 )
		{
			throw new FileDBException( "Incorrect result size, expected 1 : found " + mappables.size() );
		}
		return mappables.get( 0 );
	}
}
