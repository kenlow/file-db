package dev.stjiggyfly.filedb.requests;

import dev.stjiggyfly.filedb.core.Comparison;
import dev.stjiggyfly.filedb.core.Condition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings( "unused" )
public class DatabaseRequest
{
	private Type requestType;
	private String table;
	private List<String> selectColumns;
	private List<Condition> conditionColumns;
	private Map<String, String> updateColumns;
	private Map<String, String> insertColumns;


	/**
	 * Add columns by name to the request.
	 * This method is to be called individually for each desired column to be selected from the
	 * file database. Each column name will be added to a master list that gets sent up to the
	 * database processor.
	 *
	 * In SQL this method represents the fields in a select statement: SELECT COLUMN1, COLUMN2 FROM TABLE;
	 *
	 * @param columnName - Name of the column to be selected from the file database
	 */
	public void withSelectColumn ( String columnName )
	{
		setSelectColumns( addValue( getSelectColumns(), columnName ) );
	}

	/**
	 * Add columns and values to be updated to the request.
	 * This method is to be called individually for each desired column to be updated.
	 * Each pair of parameters will be inserted into a map that gets sent up to the
	 * database processor.
	 *
	 * In SQL this method represents the SET in an update statement: UPDATE TABLE SET COLUMN = VALUE;
	 *
	 * @param columnName - The column you would like to update in the file database
	 * @param value - The value you would like to update the column with
	 */
	public void updateColumnWithValue ( String columnName, String value )
	{
		setUpdateColumns( addValue( getUpdateColumns(), columnName, value ) );
	}

	/**
	 * Add columns and values to be inserted to the request.
	 * This method is to be called individually for each desired column to be inserted.
	 * Each pair of parameters will be added to a map that gets sent up to the database processor
	 *
	 * In SQL this method represents the VALUES in an insert statement: INSERT INTO TABLE VALUES (x, y, z);
	 *
	 * @param columnName - The column you would like to insert a value into in the file database
	 * @param value - The value you would like to insert into the corresponding column
	 */
	public void insertColumnWithValue ( String columnName, String value )
	{
		setInsertColumns( addValue( getInsertColumns(), columnName, value ) );
	}

	/**
	 * Adds a condition to the request.
	 * This method is to be called individually for each desired condition to be compared against.
	 * Each set of parameters will be addd to a list of Conditions that gets sent up tot he database processor
	 *
	 * In SQL this method represents a WHERE clause: SELECT * FROM TABLE WHERE COLUMN = VALUE;
	 *
	 * @param columnName - Column name that the value being compared with
	 * @param comparison - Comparison enum value holding the appropriate action for comparison
	 * @param value - The value the column must contain in conjunction with the comparison to provide a boolean result
	 */
	public void withCondition ( String columnName, Comparison comparison, String value )
	{
		List<Condition> conditions = getConditionColumns();
		if ( conditions == null )
		{
			conditions = new ArrayList<>();
		}
		conditions.add( new Condition( columnName, comparison, value ) );
		setConditionColumns( conditions );
	}

	private List<String> addValue ( List<String> list, String columnName )
	{
		if ( list == null )
		{
			list = new ArrayList<>();
		}
		list.add( columnName );
		return list;
	}

	private Map<String, String> addValue ( Map<String, String> map, String columnName, String value )
	{
		if ( map == null )
		{
			map = new HashMap<>();
		}
		map.put( columnName, value );
		return map;
	}

	public Type getRequestType ()
	{
		return requestType;
	}

	public void setRequestType ( Type requestType )
	{
		this.requestType = requestType;
	}

	public String getTable ()
	{
		return table;
	}

	public void setTable ( String table )
	{
		this.table = table;
	}

	public List<String> getSelectColumns ()
	{
		return selectColumns;
	}

	private void setSelectColumns ( List<String> selectColumns )
	{
		this.selectColumns = selectColumns;
	}

	public List<Condition> getConditionColumns ()
	{
		return conditionColumns;
	}

	private void setConditionColumns ( List<Condition> conditionColumns )
	{
		this.conditionColumns = conditionColumns;
	}

	public Map<String, String> getUpdateColumns ()
	{
		return updateColumns;
	}

	private void setUpdateColumns ( Map<String, String> updateColumns )
	{
		this.updateColumns = updateColumns;
	}

	public Map<String, String> getInsertColumns ()
	{
		return insertColumns;
	}

	private void setInsertColumns ( Map<String, String> insertColumns )
	{
		this.insertColumns = insertColumns;
	}

	/**
	 * This enum represents the 4 major actions for accessing a database to retrieve and manipulate data.
	 * SELECT - Retrieve information. Make heavy use of withSelectColumn and withCondition
	 * INSERT - Inserts a row into the database file. Make heavy use of insertColumnWithValue
	 * UPDATE - Update rows in database file. Make heavy use of updateColumnWithValue and withCondition
	 * DELETE - Delete specific row in database file. Make heavy use of withCondition
	 */
	public enum Type
	{
		SELECT,
		INSERT,
		UPDATE,
		DELETE
	}
}
