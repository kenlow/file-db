package dev.stjiggyfly.filedb.exceptions;

public class FileDBException extends RuntimeException
{
	public FileDBException ( String message )
	{
		super( message );
	}
}
