package dev.stjiggyfly.filedb.database;

import dev.stjiggyfly.filedb.core.ResponseStatus;
import dev.stjiggyfly.filedb.core.ResultSet;
import dev.stjiggyfly.filedb.exceptions.FileDBException;
import dev.stjiggyfly.filedb.requests.DatabaseRequest;
import dev.stjiggyfly.filedb.responses.DatabaseResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileDatabase
{
	private String dbLocation;
	private String delimiter;
	private List<FileTable> tables;

	private static final String DEFAULT_DELIMITER = "!@#";

	public FileDatabase ( String dbLocation )
	{
		this( dbLocation, DEFAULT_DELIMITER );
	}

	public FileDatabase ( String dbLocation, String delimiter )
	{
		setDbLocation( dbLocation );
		setDelimiter( delimiter );
		setTables();
	}

	private String getDbLocation ()
	{
		return dbLocation;
	}

	private void setDbLocation ( String dbLocation )
	{
		this.dbLocation = dbLocation;
	}

	public String getDelimiter ()
	{
		return delimiter;
	}

	private void setDelimiter ( String delimiter )
	{
		this.delimiter = delimiter;
	}

	private List<FileTable> getTables ()
	{
		return tables;
	}

	private void setTables ( List<FileTable> tables )
	{
		this.tables = tables;
	}

	private void setTables ()
	{
		File fileDB = new File( getDbLocation() );
		File[] files = fileDB.listFiles();
		setTables( new ArrayList<>() );
		if ( files != null )
		{
			for ( File file : files )
			{
				getTables().add( new FileTable( file.getName(), file.getPath(), getDelimiter() ) );
			}
		}
	}

	public DatabaseResponse execute ( DatabaseRequest request )
	{
		DatabaseResponse response = new DatabaseResponse();
		response.setStatus( ResponseStatus.SUCCESS );
		response.setRequest( request );
		if ( request.getTable() != null )
		{
			try
			{
				FileTable table = findTable( request.getTable() );
				switch ( request.getRequestType() )
				{
					case SELECT:
						response.setResultSet( new ResultSet( table.select( request.getSelectColumns(), request.getConditionColumns() ) ) );
						break;
					case INSERT:
						table.insert( request.getInsertColumns() );
						break;
					case UPDATE:
						table.update( request.getUpdateColumns(), request.getConditionColumns() );
					case DELETE:
						break;
					default:
						throw new FileDBException( "FileDatabase request type not found or unsupported" );
				}
			}
			catch ( FileDBException exception )
			{
				response.setStatus( ResponseStatus.FAIL );
				response.setErrorMessage( exception.getMessage() );
			}
		}
		return response;
	}

	private FileTable findTable ( String tableName )
	{
		if ( tableName == null || tableName.trim().equalsIgnoreCase( "" ) )
		{
			throw new FileDBException( "No table has been requested." );
		}
		FileTable findTable = null;
		for ( FileTable table : getTables() )
		{
			if ( table.getName().toLowerCase().startsWith( tableName.toLowerCase() ) )
			{
				findTable = table;
			}
		}
		if ( findTable == null )
		{
			throw new FileDBException( "Request table not found for table = " + tableName );
		}
		return findTable;
	}
}
