package dev.stjiggyfly.filedb.database;

import dev.stjiggyfly.filedb.core.Condition;
import dev.stjiggyfly.filedb.exceptions.FileDBException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class FileTable
{
	private String name;
	private String tablePath;
	private String delimiter;
	private List<String> masterColumns;

	FileTable ( String name, String tablePath, String delimiter )
	{
		setName( name );
		setTablePath( tablePath );
		setDelimiter( delimiter );
		try
		{
			BufferedReader reader = new BufferedReader( new FileReader( getTablePath() ) );
			setMasterColumns( Arrays.asList( reader.readLine().split( getDelimiter() ) ) );
			reader.close();
		}
		catch ( Exception exception )
		{
			exception.printStackTrace();
		}
	}

	public String getName ()
	{
		return name;
	}

	private void setName ( String name )
	{
		this.name = name;
	}

	private String getTablePath ()
	{
		return tablePath;
	}

	private void setTablePath ( String tablePath )
	{
		this.tablePath = tablePath;
	}

	public String getDelimiter ()
	{
		return delimiter;
	}

	public void setDelimiter ( String delimiter )
	{
		this.delimiter = delimiter;
	}

	private List<String> getMasterColumns ()
	{
		return masterColumns;
	}

	private void setMasterColumns ( List<String> masterColumns )
	{
		this.masterColumns = masterColumns;
	}

	public List<Map<String, String>> select ( List<String> selectColumns, List<Condition> conditions )
	{
		validateSelectRequest( selectColumns, conditions );
		List<Map<String, String>> resultRows = new ArrayList<>();
		try
		{
			List<List<String>> allRows = readTable();

			// filter all rows based on the compareColumns info. If no compareColumns info passed in, return all rows
			List<List<String>> filteredRows = new ArrayList<>();
			for ( List<String> row : allRows )
			{
				boolean isValidRow = true;
				if ( conditions != null && conditions.size() > 0 )
				{
					for ( Condition condition : conditions )
					{
						int masterColumnIndex = getMasterColumns().indexOf( condition.getColumn() );
						if ( !condition.getComparison().evaluate( row.get( masterColumnIndex ), condition.getValue() ) )
						{
							isValidRow = false;
							break;
						}
					}
				}
				if ( isValidRow )
				{
					filteredRows.add( row );
				}
			}

			// finalize columns being returned
			if ( selectColumns == null ||
				 selectColumns.size() == 0 ||
				 ( selectColumns.size() == 1 &&
				   ( selectColumns.get( 0 ).equalsIgnoreCase( "all" ) ||
					 selectColumns.get( 0 ).equalsIgnoreCase( "*" ) ) ) )
			{
				selectColumns = getMasterColumns();
			}

			// select indexes of all masterColumns being retrieved from the master column list,
			// the fields in each row should be in this same order allowing us to
			// use the indexes to select each field easily
			List<Integer> columnIndexes = new ArrayList<>();
			for ( String what : selectColumns )
			{
				columnIndexes.add( getMasterColumns().indexOf( what ) );
			}

			// Use the indices to select each field and map it with its appropriate column name
			for ( List<String> row : filteredRows )
			{
				Map<String, String> finalRow = new HashMap<>();
				for ( int columnIndex : columnIndexes )
				{
					finalRow.put( getMasterColumns().get( columnIndex ), row.get( columnIndex ) );
				}
				resultRows.add( finalRow );
			}
		}
		catch ( Exception exception )
		{
			throw new FileDBException( "Unable to open DB for table = " + getName() );
		}

		return resultRows;
	}

	public void insert ( Map<String, String> values )
	{
		validateInsertRequest( values );
		List<String> writeRow = new ArrayList<>();

		// Create row in memory, defaulting value to blank if field is not passed in
		for ( String column : getMasterColumns() )
		{
			writeRow.add( values.getOrDefault( column, "" ) );
		}
		try
		{
			// Write to file in APPEND mode to add it to the end
			Files.write( Paths.get( getTablePath() ), ( "\n" + String.join( getDelimiter(), writeRow ) ).getBytes(), StandardOpenOption.APPEND );
		}
		catch ( Exception e )
		{
			throw new FileDBException( "Cannot write to table = " + getName() );
		}
	}

	public void update ( Map<String, String> values, List<Condition> conditions )
	{
		validateUpdateRequest( values, conditions );
		List<List<String>> allRows = readTable();
		for ( List<String> row : allRows )
		{
			boolean update = true;
			if ( conditions != null )
			{
				for ( Condition condition : conditions )
				{
					int index = getMasterColumns().indexOf( condition.getColumn() );
					if ( !condition.getComparison().evaluate( row.get( index ), condition.getValue() ) )
					{
						update = false;
						break;
					}
				}
			}
			if ( update )
			{
				for ( String key : values.keySet() )
				{
					int index = getMasterColumns().indexOf( key );
					try
					{
						row.set( index, values.get( key ) );
					}
					catch ( IndexOutOfBoundsException exception )
					{
						row.add( "" );
					}
				}
			}
		}
		allRows.add( 0, getMasterColumns() );
		try
		{
			StringBuilder fileInput = new StringBuilder();
			for ( int i = 0; i < allRows.size(); i++ )
			{
				fileInput.append( String.join( getDelimiter(), allRows.get( i ) ) );
				if ( i < allRows.size() - 1 )
				{
					fileInput.append( "\n" );
				}
			}
			Files.write( Paths.get( getTablePath() ), fileInput.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING );
		}
		catch ( Exception e )
		{
			throw new FileDBException( "Cannot write to table = " + getName() );
		}
	}

	private List<List<String>> readTable ()
	{
		List<List<String>> allRows = new ArrayList<>();
		try
		{
			for ( String row : Files.lines( Paths.get( getTablePath() ) ).collect( Collectors.toList() ) )
			{
				allRows.add( new ArrayList<>( Arrays.asList( row.split( getDelimiter() ) ) ) );
			}

			//remove first row because it only contains columns
			allRows.remove( 0 );
		}
		catch ( Exception exception )
		{
			throw new FileDBException( "Cannot readTable table = " + getName() );
		}
		return allRows;
	}

	private void validateSelectRequest ( List<String> selectColumns, List<Condition> conditions )
	{
		List<String> allColumns = getMasterColumns();
		/*if ( selectColumns == null )
		{
			throw new FileDBException( "Must select at least 1 column for information." );
		}*/
		if ( selectColumns != null )
		{
			for ( String selectColumn : selectColumns )
			{
				if ( !allColumns.contains( selectColumn ) )
				{
					throw new FileDBException( "Cannot select where invalid column name = " + selectColumn );
				}
			}
		}
		if ( conditions != null )
		{
			for ( Condition condition : conditions )
			{
				if ( !allColumns.contains( condition.getColumn() ) )
				{
					throw new FileDBException( "Cannot compare where invalid column name = " + condition.getColumn() );
				}
			}
		}
	}

	private void validateInsertRequest ( Map<String, String> values )
	{
		List<String> allColumns = getMasterColumns();
		if ( values == null || values.size() == 0 )
		{
			throw new FileDBException( "Must insert at least one value" );
		}
		for ( String key : values.keySet() )
		{
			if ( !allColumns.contains( key ) )
			{
				throw new FileDBException( "Cannot insert where invalid column name = " + key );
			}
		}
	}

	private void validateUpdateRequest ( Map<String, String> values, List<Condition> conditions )
	{
		List<String> allColumns = getMasterColumns();
		if ( values == null || values.size() == 0 )
		{
			throw new FileDBException( "Must update at least one value" );
		}
		for ( String key : values.keySet() )
		{
			if ( !allColumns.contains( key ) )
			{
				throw new FileDBException( "Cannot update where invalid column name = " + key );
			}
		}
		if ( conditions != null )
		{
			for ( Condition condition : conditions )
			{
				if ( !allColumns.contains( condition.getColumn() ) )
				{
					throw new FileDBException( "Cannot update where invalid condition column name = " + condition.getColumn() );
				}
			}
		}
	}
}
