package dev.stjiggyfly.filedb.core;

import dev.stjiggyfly.filedb.exceptions.FileDBException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ResultSet
{
	private List<Map<String, String>> results;
	private Integer index;

	public ResultSet ( List<Map<String, String>> results )
	{
		this.results = results;
	}

	public boolean next ()
	{
		boolean hasNext = true;
		if ( index == null )
		{
			index = 0;
		}
		else
		{
			index++;
		}
		if ( results == null || results.size() == 0 || index >= results.size() )
		{
			hasNext = false;
		}
		return hasNext;
	}

	private String get ( String field )
	{
		return results.get( index ).get( field );
	}

	private String get ( int columnNumber )
	{
		return ( String ) results.get( index ).values().toArray()[columnNumber];
	}

	public String getString ( int columnNumber )
	{
		return get( columnNumber );
	}

	public String getString ( String field )
	{
		return get( field );
	}

	public Integer getInt ( int columnNumber )
	{
		return Integer.parseInt( get( columnNumber ) );
	}

	public Integer getInt ( String field )
	{
		return Integer.parseInt( get( field ) );
	}

	public Date getDate ( int columnNumber, String dbFormat )
	{
		String dateStr = get( columnNumber );
		DateFormat format = new SimpleDateFormat( dbFormat );
		Date returnDate = null;
		try
		{
			returnDate = format.parse( dateStr );
		}
		catch ( ParseException e )
		{
			throw new FileDBException( "Unable to create Date for " + dateStr + " and format " + dbFormat );
		}

		return returnDate;
	}

	public Date getDate ( String field, String dbFormat )
	{
		String dateStr = get( field );
		DateFormat format = new SimpleDateFormat( dbFormat );
		Date returnDate = null;
		try
		{
			returnDate = format.parse( dateStr );
		}
		catch ( ParseException e )
		{
			throw new FileDBException( "Unable to create Date for " + dateStr + " and format " + dbFormat );
		}

		return returnDate;
	}

	public BigDecimal getBigDecimal ( int columnNumber )
	{
		return new BigDecimal( get( columnNumber ) );
	}

	public BigDecimal getBigDecimal ( String field )
	{
		return new BigDecimal( get( field ) );
	}

	public Boolean getBoolean ( int columnNumber )
	{
		String value = get( columnNumber );
		return value.equals( "1" ) || value.equalsIgnoreCase( "true" );
	}

	public Boolean getBoolean ( String field )
	{
		String value = get( field );
		return value.equals( "1" ) || value.equalsIgnoreCase( "true" );
	}
}
