package dev.stjiggyfly.filedb.core;

public class Condition
{
	private String column;
	private Comparison comparison;
	private String value;

	public Condition ( String column, Comparison comparison, String value )
	{
		setColumn( column );
		setComparison( comparison );
		setValue( value );
	}

	public String getColumn ()
	{
		return column;
	}

	public void setColumn ( String column )
	{
		this.column = column;
	}

	public Comparison getComparison ()
	{
		return comparison;
	}

	public void setComparison ( Comparison comparison )
	{
		this.comparison = comparison;
	}

	public String getValue ()
	{
		return value;
	}

	public void setValue ( String value )
	{
		this.value = value;
	}
}
