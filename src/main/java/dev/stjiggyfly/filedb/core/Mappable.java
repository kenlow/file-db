package dev.stjiggyfly.filedb.core;

public interface Mappable
{
	Mappable map ( ResultSet resultSet );
}
