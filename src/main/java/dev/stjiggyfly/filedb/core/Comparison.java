package dev.stjiggyfly.filedb.core;

public enum Comparison
{
	//todo: between, inList, isNull, notNull
	Equals
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean isEqual = false;
			if ( a != null && b != null )
			{
				if ( a.equals( b ) )
				{
					isEqual = true;
				}
			}
			return isEqual;
		}
	},
	EqualsIgnoreCase
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean isEqual = false;
			if ( a != null && b != null )
			{
				if ( a.equalsIgnoreCase( b ) )
				{
					isEqual = true;
				}
			}
			return isEqual;
		}
	},
	NotEqual
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean isNotEqual = true;
			if ( a != null && b != null )
			{
				if ( a.equals( b ) )
				{
					isNotEqual = false;
				}
			}
			return isNotEqual;
		}
	},
	NotEqualIgnoreCase
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean isNotEqual = true;
			if ( a != null && b != null )
			{
				if ( a.equalsIgnoreCase( b ) )
				{
					isNotEqual = false;
				}
			}
			return isNotEqual;
		}
	},
	Contains
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean doesContain = false;
			if ( a != null && b != null )
			{
				if ( a.contains( b ) )
				{
					doesContain = true;
				}
			}
			return doesContain;
		}
	},
	StartsWith
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean doesStartWith = false;
			if ( a != null && b != null )
			{
				if ( a.startsWith( b ) )
				{
					doesStartWith = true;
				}
			}
			return doesStartWith;
		}
	},
	EndsWith
	{
		@Override
		public boolean evaluate ( String a, String b )
		{
			boolean doesEndWith = false;
			if ( a != null && b != null )
			{
				if ( a.startsWith( b ) )
				{
					doesEndWith = true;
				}
			}
			return doesEndWith;
		}
	};

	public abstract boolean evaluate ( String a, String b );
}
